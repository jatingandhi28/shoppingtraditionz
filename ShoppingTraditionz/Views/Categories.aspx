﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ShopTraditionz.Master" AutoEventWireup="true" CodeBehind="Categories.aspx.cs" Inherits="ShoppingTraditionz.Views.Categories1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="container" style="margin-left: 70px;">
			<div class="pull-right">
				<div class="btn-group">
					<a class="btn" href="#">Sort Result</a>
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="#"><b>New Arrival</b></a></li>
						<li><a href="#">Price : low to High</a></li>
						<li><a href="#">Price : High to Low</a></li>
					</ul>
				</div>
			</div>
			<legend>Footwear</legend>
			<div class="category">
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/toystory.jpg">
					<a href="ProductDetails.aspx"><button class="btn btn-primary buyNow" type="button">View Details</button></a>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/toystory.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/toystory.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/toystory.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/toystory.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<legend>Jewellary</legend>
			<div class="category">
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/nemo.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/nemo.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/nemo.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/nemo.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/nemo.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/nemo.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<legend>Home Decor</legend>
			<div class="category">
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/up.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/up.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/up.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<legend>Bags</legend>
			<div class="category">
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/walle.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/walle.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/walle.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/walle.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
				<div class="span3 categoryview" style="width: 360px;">
					<img class="img-polaroid img" src="../img/walle.jpg">
					<button class="btn btn-primary buyNow" type="button">View Details</button>
					<div class="description span3">
						<div class="description-content span2 pull-left">
							<h4>skybags backpack</h4>
						</div>
						<div class="description-content span1 pull-right">
							<h4>$ 20.00</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
</asp:Content>
