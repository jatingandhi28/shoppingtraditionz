﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ShoppingTraditionz.BusinessEntity
{
   public class ProductBusinessEntity
    {
       string _productId;
       string _supplierProductId;
       string _productName;
       string _productDescription;
       string _supplierId;
       string _categoryId;
       string _subCategoryId;
       string _quantityPerUnit;
       string _unitSize;
       string _unitPrice;
       string _msrp;
       string _availableSize;
       string _availColors;
       string _discount;
       string _untiWeight;
       string _unitsInStock;
       string _reorderLevel;
       string _productAvailable;
       string _discountAvailable;
       string _currentOrder;
       string _ranking;
       string _note;
       string _actionerId;
       string _actionTime;
       //[DataOrder = 1]
       public string ProductID
       {
           get { return _productId; }
           set { _productId = value; }
       }
       public string SupplierProductId
       {
           get { return _supplierProductId; }
           set { _supplierProductId = value; }
       }
       public string ProductName
       {
           get { return _productName; }
           set { _productName = value; }
       }

    }
}
